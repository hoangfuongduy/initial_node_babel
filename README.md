# UID initial setup of node express app with babel eslint configured

###### First you clone this repository to your local machine

###### Then remove the current remote by running

    git remote rm origin

###### Whenever you want to add a remote, feel free to do it yourself

###### Run command to install depedencies

    yarn install

###### Go to src/server/app.js

###### Uncomment these two lines if you want to use authentication route

    // import { users as auth } from '../users';
    *
    *
    *
    // app.use('/api/v1/auth', auth);

###### Start the development process by running

    yarn run init
    yarn start:dev

###### Enjoy Coding! :)
