import config from 'config';
// import dotenv from 'dotenv';

import createServer from './createServer';
import { connectDatabase, createDirectory, createFile } from '../utils';

// dotenv.config({ path: `${process.cwd()}/configs/.env` });
// const { NODE_ENV: env } = process.env;
const { port } = config.get('app');

const startServer = async () => {
  try {
    // below if block should be uncommented if we use mongodb atlas on production env
    // if (env !== 'production') {
    // create logs directory
    createDirectory(`${process.cwd()}/logs`);
    // create mongo.log file
    createFile(`${process.cwd()}/logs`, 'mongo.log', '');
    // create mongodb directory to store db
    createDirectory(`${process.cwd()}/mongodb`);
    // connect Database
    // }

    connectDatabase();
    // genrate the servers
    createServer(port);
  } catch (error) {
    console.error(error);
  }
};

export default startServer;
