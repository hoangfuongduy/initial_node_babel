import express from 'express';
import logger from 'morgan';
import dotenv from 'dotenv';

// import { users as auth } from '../users';
import errorHandler from '../middlewares/error';

dotenv.config({ path: `${process.cwd()}/configs/.env` });

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger('dev'));
// app.use(
//   cors({
//     origin:
//       process.env.NODE_ENV === 'production'
//         ? ['http://212.24.102.19:1114']
//         : '*',
//   }),
// );
// app.use(fileupload());

// app.use('/api/v1/auth', auth);

app.use(errorHandler);

export default app;
