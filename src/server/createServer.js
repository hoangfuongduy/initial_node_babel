/* eslint-disable import/no-extraneous-dependencies */
import http from 'http';
import app from './app';

const createServer = (httpPort) => {
  const server = http.createServer(app);
  server.listen(httpPort);
  console.log(`Start service at ${httpPort}`);
};

export default createServer;
