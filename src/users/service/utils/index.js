import jwt from 'jsonwebtoken';

export const sendTokenResponse = (user, statusCode, res) => {
  // create token
  const token = user.getSignJwtToken();
  res.status(statusCode).json({
    success: true,
    token,
    username: user.username,
    role: user.role,
  });
};
export const verifyToken = (token) => {
  const res = jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      if (err.message === 'jwt expired') return { err: 'jwt expired' };
      return false;
    }
    return decoded;
  });
  return res;
};
