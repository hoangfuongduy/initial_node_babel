/* eslint-disable no-unused-vars */

import Users from '../model/user.model';
import asyncHandler from '../../middlewares/async';
import { verifyToken, sendTokenResponse } from './utils';
import { ErrorResponse } from '../../utils';

export const register = asyncHandler(async (req, res, next) => {
  const { username, password, role } = req.body;
  const user = await Users.create({ username, password, role });
  return sendTokenResponse(user, 200, res);
});

export const login = asyncHandler(async (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return next(new ErrorResponse('Please provide name and password', 400));
  }
  // check user existed;
  const user = await Users.findOne({ username }).select('+password');
  if (!user) {
    return next(new ErrorResponse('Invalid credential', 401));
  }
  const isMatch = await user.matchPassword(password);
  if (!isMatch) {
    return next(new ErrorResponse('Invalid credential', 401));
  }
  return sendTokenResponse(user, 200, res);
});

export const checkValidToken = asyncHandler(async (req, res, next) => {
  const {
    headers: { authorization },
  } = req;
  const token = authorization ? authorization.split(' ')[1] : null;
  if (!token) return res.status(401).send({ err: 'No token sent' });
  const result = verifyToken(token);
  if ((result.err && result.err === 'jwt expired') || !result) {
    return res.status(401).send({ err: 'Token expired' });
  }
  const { id } = result;
  const existedUser = await Users.findById(id);
  if (!existedUser) {
    return res.sendStatus(401);
  }
  return res.status(200).json({ user: existedUser });
});

export const updatePassword = asyncHandler(async (req, res, next) => {
  const user = await Users.findById(req.user._id).select('+password');
  // check current password
  if (!(await user.matchPassword(req.body.currentPassword))) {
    return next(new ErrorResponse('Current password incorrect', 401));
  }
  user.password = req.body.newPassword;
  await user.save();
  return sendTokenResponse(user, 200, res);
});
