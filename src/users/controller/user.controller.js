import express from 'express';

import {
  register,
  login,
  updatePassword,
  checkValidToken,
} from '../service/user.service';

import { protect } from '../../middlewares/auth';

const router = express.Router();

router.post('/register', register);
router.post('/login', login);
router.route('/update-password').put(protect, updatePassword);
router.get('/check-validity', checkValidToken);

export default router;
