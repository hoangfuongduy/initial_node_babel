import { Schema } from 'mongoose';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

const UserSchema = new Schema(
  {
    username: {
      type: String,
      required: [true, 'User must have a name'],
      unique: true,
    },
    role: { type: String, enum: ['user', 'manager'], default: 'user' },
    password: {
      type: String,
      required: [true, 'Please provide user password'],
      minlength: 8,
      select: false,
    },
  },
  { timestamps: true },
);
// encrypt password with bryptjs
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});
// sign JWT and return
UserSchema.methods.getSignJwtToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};
// match password when login -- hashpassword
UserSchema.methods.matchPassword = async function (enteredPassword) {
  return bcrypt.compare(enteredPassword, this.password);
};

export default UserSchema;
