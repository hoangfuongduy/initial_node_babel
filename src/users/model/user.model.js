import mongoose from 'mongoose';

import UserSchema from '../schema/user.schema';

export default mongoose.model('users', UserSchema);
