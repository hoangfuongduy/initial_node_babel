import mongoose from 'mongoose';
import config from 'config';
import fs from 'fs';
import dotenv from 'dotenv';

dotenv.config({ path: `${process.cwd()}/configs/.env` });

const { NODE_ENV: env, MONGO_URI: productionMongoUri } = process.env;
// as if we use mongodb atlas we store the uri in .env file so that it's safe

const databaseConfig = config.get('database');
const { port, name } = databaseConfig;

const connectDatabase = () => {
  let uri;

  if (env === 'production') {
    // connect to production mongo uri
    uri = productionMongoUri || `mongodb://localhost:${port}/${name}`;
  } else {
    // connection to development mongo uri
    uri = `mongodb://localhost:${port}/${name}`;
  }
  mongoose.connect(uri, (err) => {
    if (err) {
      console.log(err);
      throw err;
    }
    console.log(`Successfully connected to MongoDB on ${port}`);
  });
};

class ErrorResponse extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
  }
}

const createDirectory = (path) => {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }
};

const createFile = (path, filename, data = '') => {
  createDirectory(path);
  const filePath = `${path}/${filename}`;
  if (!fs.existsSync(filePath)) {
    fs.writeFileSync(filePath, data);
  }
};

const removeAllFiles = (path) => {
  const allFiles = fs.readdirSync(path);
  for (let index = 0; index < allFiles.length; index += 1) {
    const filename = allFiles[index];
    fs.unlinkSync(`${path}/${filename}`);
  }
  return true;
};

export {
  ErrorResponse,
  createDirectory,
  createFile,
  removeAllFiles,
  connectDatabase,
};
