/* eslint-disable arrow-body-style */
import jwt from 'jsonwebtoken';

import Users from '../users/model/user.model';

import { ErrorResponse } from '../utils';

import asyncHandler from './async';
// Protect routes;
export const protect = asyncHandler(async (req, res, next) => {
  let token;
  if (
    // eslint-disable-next-line operator-linebreak
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    // eslint-disable-next-line prefer-destructuring
    token = req.headers.authorization.split(' ')[1];
  }
  if (!token) {
    return next(new ErrorResponse('Not authorized to access this routes', 401));
  }
  try {
    // verify token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await Users.findOne({ _id: decoded.id });
    return next();
  } catch (error) {
    return next(new ErrorResponse('Not authorized to access this routes', 401));
  }
});

// Grant access to specific role
export const authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new ErrorResponse(
          `User role ${req.user.role} is unauthorized to access this routes`,
          403,
        ),
      );
    }
    return next();
  };
};
