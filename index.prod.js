import 'core-js/stable';
import 'regenerator-runtime/runtime';

import startServer from './src/server/server';

startServer();
