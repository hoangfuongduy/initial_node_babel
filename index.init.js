import 'core-js/stable';
import 'regenerator-runtime/runtime';

import { createDirectory, createFile } from './src/utils';

createDirectory(`${process.cwd()}/logs`);
// create mongo.log file
createFile(`${process.cwd()}/logs`, 'mongo.log', '');
// create mongodb directory to store db
createDirectory(`${process.cwd()}/mongodb`);

process.exit();
